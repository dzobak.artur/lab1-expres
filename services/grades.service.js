const mockData = require('../helpers/mock-data');

function _generateId() {
    const crypto = require("crypto");
    return crypto.randomBytes(16).toString("hex");
}

async function createGrade(grade) {
    const newGrade = { id: _generateId(), ...grade };
    mockData.grades.push(newGrade);

    return newGrade;
}

async function getGrades({ searchString = '', page = 1, perPage = Number.MAX_SAFE_INTEGER }) {
    searchString = searchString?.toLowerCase();
    const searchResult = mockData.grades.filter(g => g.subject?.toLowerCase().includes(searchString));

    return {
        items: searchResult.slice((page - 1)*perPage, page * perPage),
        count: searchResult.length,
    }
}

async function getGrade(gradeId) {
    return mockData.grades.find(g => g.id == gradeId);
}

async function updateGrade(gradeId, gradeData) {
    const index = mockData.grades.findIndex(g => g.id === gradeId);

    if (index === -1) return;

    const updatedGrade = { ...mockData.grades[index], ...gradeData, id: gradeId };

    mockData.grades[index] = updatedGrade;
};

async function deleteGrade(gradeId) {
    mockData.grades = mockData.grades.filter(g => g.id != gradeId);
};

module.exports = {
    createGrade,
    getGrades,
    getGrade,
    updateGrade,
    deleteGrade,
};
