require('dotenv').config();

const config = {
    port: process.env.PORT || 3007,
};

module.exports = config;